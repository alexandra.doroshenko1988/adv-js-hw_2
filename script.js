// Теоретичне питання
// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію `try...catch`.


// Робота з мережевими запитами: При виконанні мережевих запитів можуть виникати різноманітні помилки, такі як недоступність сервера, проблеми з підключенням до Інтернету, некоректні дані, які приходять від сервера і т. д. Використання try...catch дозволяє перехоплювати ці помилки та вживати необхідні заходи для їх обробки.

// Робота з базами даних: Під час роботи з базами даних можуть виникати помилки, такі як проблеми з підключенням до бази даних, некоректні запити і т. д. Використання try...catch допомагає перехоплювати ці помилки та реагувати на них відповідним чином.

// Використання сторонніх бібліотек: При використанні сторонніх бібліотек можуть виникати різні проблеми, такі як невірні вхідні дані, помилки в самій бібліотеці і т. д. Використання try...catch дозволяє перехоплювати ці помилки та здійснювати відповідні дії для їх вирішення.


const books = [
    { 
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70 
    }, 
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    }, 
    { 
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    }, 
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const root = document.getElementById("root");

function isValidBook(book) {
    return book.author !== undefined && book.name !== undefined && book.price !== undefined;
}

function createList(books) {
    const ul = document.createElement("ul");

    books.forEach(book => {
        try {
            if (isValidBook(book)) {
                const li = document.createElement("li");
                li.textContent = `${book.name} by ${book.author}, Price: ${book.price}`;
                ul.appendChild(li);
            } else {
                throw new Error("Invalid book properties");
            }
        } catch (error) {
            console.error(error.message);
        }
    });

    return ul;
}

const booksList = createList(books);
root.appendChild(booksList);